package com.iman.optusapplication2;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.iman.optusapplication2.data.TravelInfo;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class DownloadTask extends AsyncTask<Void, Void, String> {
    public interface Callback {
        void onComplete(TravelInfo[] travelInfo);

        void onFailure(String reason);
    }

    private TravelInfo[] travelInfo;
    private Set<Callback> callbacks = new HashSet<>();

    public DownloadTask addObserver(Callback cb) {
        callbacks.add(cb);
        return this;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {
        Thread.currentThread().setName(this.toString());

        String ret = "";

        HttpURLConnection urlConnection = null;
        InputStream       is            = null;
        try {
            URL url;
            url = new URL("http://express-it.optusnet.com.au/sample.json");

            urlConnection = (HttpURLConnection) url.openConnection();
            String        result;
            StringBuilder sb = new StringBuilder();

            is = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String         inputLine;
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            result = sb.toString();
            Gson gson = new Gson();
            travelInfo = gson.fromJson(result, TravelInfo[].class);
            ret = "OK";
        } catch (Exception e) {
            ret = e.getMessage();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    if (ret.length() > 0) {
                        ret += "\n";
                    }
                    ret += e.getMessage();
                }
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return ret;
    }

    @Override
    protected void onPostExecute(String result) {
        if (result.equals("OK")) {
            for (Callback cb : callbacks) {
                cb.onComplete(travelInfo);
            }
        } else {
            for (Callback cb : callbacks) {
                cb.onFailure(result);
            }
        }
        super.onPostExecute(result);
    }
}
