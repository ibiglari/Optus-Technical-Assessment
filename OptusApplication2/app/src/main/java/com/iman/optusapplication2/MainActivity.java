package com.iman.optusapplication2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.iman.optusapplication2.data.TravelInfo;

public class MainActivity extends AppCompatActivity {
    private Spinner      spSource;
    private TextView     tvTransport;
    private MapView      mapView;
    private GoogleMap    googleMap;
    private TravelInfo[] mTravelInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spSource = (Spinner) findViewById(R.id.sp_source);
        spSource.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    String s   = mTravelInfo[position].fromcentral.car;
                    String txt = "";
                    if (!TextUtils.isEmpty(s)) {
                        txt = String.format(getResources().getString(R.string.car), s);
                    }
                    s = mTravelInfo[position].fromcentral.train;
                    if (!TextUtils.isEmpty(s)) {
                        txt = String.format(getResources().getString(R.string.train), txt, TextUtils.isEmpty(txt) ? "" : "\n", s);
                    }
                    tvTransport.setText(txt);
                } catch (Exception e) {
                    tvTransport.setText(getResources().getString(R.string.info_not_available));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                tvTransport.setText("");
            }
        });

        tvTransport = (TextView) findViewById(R.id.tv_transport);
        MapsInitializer.initialize(this);
        mapView = (MapView) findViewById(R.id.map_view);
        assert mapView != null;
        mapView.onCreate(savedInstanceState);
        Button btn = (Button) findViewById(R.id.btn_navigate);
        assert btn != null;
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedItem = spSource.getSelectedItemPosition();
                if (selectedItem >= 0) {
                    LatLng l = new LatLng(mTravelInfo[selectedItem].location.latitude, mTravelInfo[selectedItem].location.longitude);
                    googleMap.clear();
                    googleMap.addMarker(new MarkerOptions().position(l).title(mTravelInfo[selectedItem].name));
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(l).zoom(15).build()));
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                MainActivity.this.googleMap = googleMap;

                googleMap.getUiSettings().setZoomGesturesEnabled(false);
                googleMap.getUiSettings().setCompassEnabled(false);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);
                googleMap.getUiSettings().setScrollGesturesEnabled(false);
                googleMap.setMyLocationEnabled(false);
            }
        });

        DownloadTask downloadTask = new DownloadTask();
        downloadTask.addObserver(new DownloadTask.Callback() {
            @Override
            public void onComplete(TravelInfo[] travelInfo) {
                mTravelInfo = travelInfo;
                ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_item);
                for (TravelInfo aTravelInfo : mTravelInfo) {
                    adapter.add(aTravelInfo.name);
                }
                spSource.setAdapter(adapter);
            }

            @Override
            public void onFailure(String reason) {
                Toast.makeText(MainActivity.this, String.format(getResources().getString(R.string.comm_error), reason), Toast.LENGTH_LONG).show();
            }
        }).execute();
    }
}
