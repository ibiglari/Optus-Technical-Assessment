package com.iman.optusapplication2.data;

public class TravelInfo {
    public int         id;
    public String      name;
    public FromCentral fromcentral;
    public Location    location;
}
