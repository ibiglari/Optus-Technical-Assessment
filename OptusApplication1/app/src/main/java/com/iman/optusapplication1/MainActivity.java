package com.iman.optusapplication1;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

public class MainActivity extends AppCompatActivity {
    public static final String MY_STRING = "MyString";
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.tvSelectedItem);
        if (savedInstanceState != null)
            textView.setText(savedInstanceState.getString(MY_STRING));

        ViewPager pager = (ViewPager) findViewById(R.id.view_pager);
        assert pager != null;
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

        CirclePageIndicator circlePageIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        assert circlePageIndicator != null;
        circlePageIndicator.setViewPager(pager);
        circlePageIndicator.setFillColor(Color.BLUE);
        circlePageIndicator.setStrokeColor(Color.BLUE);

        Button btn = (Button) findViewById(R.id.item1);
        assert btn != null;
        ViewGroup.LayoutParams p = btn.getLayoutParams();
        p.width = (int) (getScreenWidth() * 0.33);
        btn.setLayoutParams(p);

        btn = (Button) findViewById(R.id.item2);
        assert btn != null;
        p = btn.getLayoutParams();
        p.width = (int) (getScreenWidth() * 0.33);
        btn.setLayoutParams(p);

        btn = (Button) findViewById(R.id.item3);
        assert btn != null;
        p = btn.getLayoutParams();
        p.width = (int) (getScreenWidth() * 0.33);
        btn.setLayoutParams(p);

        btn = (Button) findViewById(R.id.item4);
        assert btn != null;
        p = btn.getLayoutParams();
        p.width = (int) (getScreenWidth() * 0.33);
        btn.setLayoutParams(p);

        btn = (Button) findViewById(R.id.item5);
        assert btn != null;
        p = btn.getLayoutParams();
        p.width = (int) (getScreenWidth() * 0.33);
        btn.setLayoutParams(p);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString(MY_STRING, textView.getText().toString());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        textView.setText(savedInstanceState.getString(MY_STRING));
    }

    public void onItemClick(View v) {
        textView.setText(((Button) v).getText());
    }

    public void onColorButtonClick(View v) {
        View view = findViewById(R.id.llColorBox);
        assert view != null;
        view.setBackgroundColor(Color.parseColor(((Button) v).getText().toString()));
    }

    private int getScreenWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        int            width;
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;
        return width;
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            return PagerFragment.newInstance(MainActivity.this, pos + 1);
        }

        @Override
        public int getCount() {
            return 4;
        }
    }
}
