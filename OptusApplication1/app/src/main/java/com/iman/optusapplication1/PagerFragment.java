package com.iman.optusapplication1;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class PagerFragment extends Fragment {
    public static final String MY_STRING = "MyString";
    private String mFragmentText;

    public PagerFragment() {

    }

    public static PagerFragment newInstance(Context ctx, int number) {
        PagerFragment fragment = new PagerFragment();
        fragment.mFragmentText = String.format(ctx.getResources().getString(R.string.fragment_name), number);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pager, container, false);
        if (savedInstanceState != null)
            mFragmentText = savedInstanceState.getString(MY_STRING);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), mFragmentText, Toast.LENGTH_SHORT).show();
            }
        });
        ((TextView) view.findViewById(R.id.tv_fragmentNumber)).setText(mFragmentText);
        return view;
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString(MY_STRING, mFragmentText);
        super.onSaveInstanceState(savedInstanceState);
    }
}
